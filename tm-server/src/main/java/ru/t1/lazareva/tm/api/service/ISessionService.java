package ru.t1.lazareva.tm.api.service;

import ru.t1.lazareva.tm.model.Session;

public interface ISessionService extends IUserOwnedService<Session> {
}
