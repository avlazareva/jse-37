package ru.t1.lazareva.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.api.DBConstants;
import ru.t1.lazareva.tm.api.repository.IUserOwnedRepository;
import ru.t1.lazareva.tm.model.AbstractUserOwnedModel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    @NotNull
    private String USER_ID_COLUMN = DBConstants.COLUMN_USER_ID;

    public AbstractUserOwnedRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @Override
    public void clear(@NotNull final String userId) throws Exception {
        final List<M> models = findAll(userId);
        removeAll(models);
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final String userId) throws Exception {
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE %s = ?", getTableName(), USER_ID_COLUMN);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                result.add(fetch(resultSet));
            }
        }
        return result;
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final String userId, @NotNull final Comparator<M> comparator) throws Exception {
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull String sql = String.format("SELECT * FROM %s WHERE %s = ?", getTableName(), USER_ID_COLUMN);
        if (comparator != null) sql = String.format(sql + " ORDER BY %s", getSortType(comparator));
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                result.add(fetch(resultSet));
            }
        }
        return result;
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) throws Exception {
        return findOneById(userId, id) != null;
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String userId, @Nullable String id) throws Exception {
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE id = ? AND %s = ? LIMIT 1", getTableName(), USER_ID_COLUMN);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, id);
            statement.setString(2, userId);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            return fetch(resultSet);
        }
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable final String userId, @Nullable Integer index) throws Exception {
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE %s = ? LIMIT ?", getTableName(), USER_ID_COLUMN);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.setInt(2, index);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            for (int i = 0; i < index - 1; i++) {
                resultSet.next();
            }
            return fetch(resultSet);
        }
    }

    @Override
    public int getSize(@NotNull final String userId) throws Exception {
        @NotNull final String sql = String.format("SELECT COUNT(*) FROM %s WHERE %s = ?", getTableName(), USER_ID_COLUMN);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            return resultSet.getInt("count");
        }
    }

    @Nullable
    @Override
    public M removeById(@Nullable final String userId, @Nullable final String id) throws Exception {
        @Nullable final M resultModel = findOneById(userId, id);
        if (resultModel == null) return null;
        @NotNull final String tableName = getTableName();
        @NotNull final String sql = String.format("delete from %s where id = ? and user_id = ? ;", tableName);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, id);
            statement.setString(2, userId);
            statement.executeUpdate();
        }
        return resultModel;
    }

    @Nullable
    @Override
    public M removeByIndex(@Nullable final String userId, @Nullable final Integer index) throws Exception {
        @Nullable final M resultModel = findOneByIndex(userId, index);
        if (resultModel == null) return null;
        @NotNull final String tableName = getTableName();
        @NotNull final String sql = String.format("delete from %s where index = ? and user_id = ? ;", tableName);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, index);
            statement.setString(2, userId);
            statement.executeUpdate();
        }
        return resultModel;
    }

    @Nullable
    @Override
    public M add(@Nullable final String userId, @NotNull final M model) throws Exception {
        model.setUserId(userId);
        return add(model);
    }

    @Nullable
    @Override
    public M remove(@Nullable final String userId, @Nullable final M model) throws Exception {
        if (userId == null || model == null) return null;
        return removeById(userId, model.getId());
    }

}