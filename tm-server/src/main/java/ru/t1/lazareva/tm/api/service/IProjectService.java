package ru.t1.lazareva.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.enumerated.Status;
import ru.t1.lazareva.tm.model.Project;

public interface IProjectService extends IUserOwnedService<Project> {

    @NotNull
    @SuppressWarnings("UnusedReturnValue")
    Project changeProjectStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status) throws Exception;

    @NotNull
    @SuppressWarnings("UnusedReturnValue")
    Project changeProjectStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status) throws Exception;

    @NotNull
    @SuppressWarnings("UnusedReturnValue")
    Project updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description) throws Exception;

    @NotNull
    @SuppressWarnings("UnusedReturnValue")
    Project updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description) throws Exception;

    @NotNull
    Project create(@Nullable String userId, @Nullable String name, @Nullable String description) throws Exception;

    @NotNull
    Project create(@Nullable String userId, @Nullable String name) throws Exception;

}