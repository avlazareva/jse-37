package ru.t1.lazareva.tm.exception.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.lazareva.tm.exception.AbstractException;

public abstract class AbstractUserException extends AbstractException {

    @SuppressWarnings("unused")
    public AbstractUserException() {
    }

    public AbstractUserException(@NotNull final String message) {
        super(message);
    }

    @SuppressWarnings("unused")
    public AbstractUserException(@NotNull final String message, @NotNull final Throwable cause) {
        super(message, cause);
    }

    @SuppressWarnings("unused")
    public AbstractUserException(@NotNull final Throwable cause) {
        super(cause);
    }

    @SuppressWarnings("unused")
    public AbstractUserException(@NotNull final String message, @NotNull final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}