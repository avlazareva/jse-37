package ru.t1.lazareva.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public abstract class AbstractUserOwnedModel extends AbstractModel {

    @Nullable
    private String userId;

}