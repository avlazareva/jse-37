package ru.t1.lazareva.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.lazareva.tm.model.Project;

import java.sql.ResultSet;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

    @NotNull
    Project fetch(@NotNull ResultSet row) throws Exception;

    void update(@NotNull Project project) throws Exception;

    @NotNull
    Project create(@NotNull String userId, @NotNull String name, @NotNull String description) throws Exception;

    @NotNull
    Project create(@NotNull String userId, @NotNull String name) throws Exception;

}