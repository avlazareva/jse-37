package ru.t1.lazareva.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.enumerated.Role;
import ru.t1.lazareva.tm.model.User;

public interface IUserService extends IService<User> {

    @NotNull
    User create(@Nullable String login, @Nullable String password) throws Exception;

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable String email) throws Exception;

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable Role role) throws Exception;

    @Nullable
    User findByLogin(@Nullable String login) throws Exception;

    @Nullable
    User findByEmail(@Nullable String email) throws Exception;

    @Nullable
    @SuppressWarnings("UnusedReturnValue")
    User removeByLogin(@Nullable String login) throws Exception;

    @Nullable
    @SuppressWarnings("unused")
    User removeByEmail(@Nullable String email) throws Exception;

    @NotNull
    @SuppressWarnings("UnusedReturnValue")
    User setPassword(@Nullable String id, @Nullable String password) throws Exception;

    @NotNull
    @SuppressWarnings("UnusedReturnValue")
    User updateUser(@Nullable String id, @Nullable String firstName, @Nullable String lastName, @Nullable String middleName) throws Exception;

    @NotNull
    Boolean isLoginExist(@Nullable String login) throws Exception;

    @NotNull
    Boolean isEmailExist(@Nullable String email) throws Exception;

    void lockUserByLogin(@Nullable String login) throws Exception;

    void unlockUserByLogin(@Nullable String login) throws Exception;

}