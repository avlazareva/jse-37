package ru.t1.lazareva.tm.api.service;

import java.sql.Connection;

public interface IConnectionService {

    Connection getConnection();

}
